<?php

	require_once '../Models/Motorcycle.php';
	require_once '../Models/Order.php';

	$response = array();
	$action = $_POST["action"];
	//$action = "MOTOR";
	$mtr = new Motorcycle();
	$ordr = new Order();
	if ($action == "BRAND")
	{
		$output = array();
		$result = $mtr->selectBrand();

		while($row = $result->fetch_assoc())
		{
			$output[] = $row;
		}

		$response['message'] = $output;
		$response['success'] = 1; 

	}
	if ($action == "MODEL")
	{
		$output = array();
		$mtr->_mtrBrand = $_POST["mtrBrand"];
		$result = $mtr->selectModel();

		while($row = $result->fetch_assoc())
		{
			$output[] = $row;
		}

		$response['message'] = $output;
		$response['success'] = 1; 
	}
	if ($action == "MOTOR")
	{
		$output = array();
		$result = $mtr->selectMotor();

		while($row = $result->fetch_assoc())
		{
			$output[] = $row;
		}

		$response['message'] = $output;
		$response['success'] = 1; 
	}
	if ($action == "ORDER")
	{
		$username = $_POST["username"];
		$output = array();
		$result = $ordr->getOrder($username);

		while($row = $result->fetch_assoc())
		{
			$output[] = $row;
		}

		$response['message'] = $output;
		$response['success'] = 1; 
	}
	echo json_encode($response);

?>